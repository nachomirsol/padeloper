'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var api_client = void 0;

(function () {
    var axios = require('axios');
    //const baseUrl = 'http://localhost:5000/api'
    var baseUrl = 'https://whispering-hollows-28610.herokuapp.com/api';

    api_client = {

        registerUser: function registerUser(name, surname, email, username, password) {
            return axios.post(baseUrl + '/user', { name: name, surname: surname, email: email, username: username, password: password }).then(function (res) {
                return res.data.data;
            });
        },

        loginUser: function loginUser(email, username, password) {
            return axios.post(baseUrl + '/userlogin', { email: email, username: username, password: password }).then(function (res) {
                return res.data.data;
            });
        },

        retrieveUser: function retrieveUser(idUser) {
            return axios.get(baseUrl + '/user/' + idUser);
        },

        listLeagues: function listLeagues() {
            return axios.get(baseUrl + '/leagues').then(function (res) {
                return res.data.data;
            });
        },

        listUserLeagues: function listUserLeagues(idUser) {
            return axios.get(baseUrl + '/leagues/userleagues/' + idUser).then(function (res) {
                return res.data.data;
            });
        },

        searchLeagues: function searchLeagues(query) {
            return axios.get(baseUrl + '/leagues/' + query).then(function (res) {
                return res.data.data;
            });
        },

        retrieveLeague: function retrieveLeague(id) {
            return axios.get(baseUrl + '/league/' + id).then(function (res) {
                return res.data.data;
            });
        },

        registerLeague: function registerLeague(id, name, city, club, category, type, date, maxplayers) {
            return axios.post(baseUrl + '/league', { id: id, name: name, city: city, club: club, category: category, type: type, date: date, maxplayers: maxplayers }).then(function (res) {
                return res.data.data;
            });
        },

        removeLeague: function removeLeague(idLeague) {
            return axios.delete(baseUrl + '/league/' + idLeague + '/remove-league').then(function (res) {
                return res.data;
            });
        },

        updateLeague: function updateLeague(idLeague, name, city, club, category, type, maxplayers) {
            return axios.put(baseUrl + '/league/' + idLeague + '/update-league', { name: name, city: city, club: club, category: category, type: type, maxplayers: maxplayers }).then(function (res) {
                return res.data;
            });
        },

        removePlayerFromLeague: function removePlayerFromLeague(idLeague, idPlayer) {
            return axios.put(baseUrl + '/league/' + idLeague + '/remove-player/' + idPlayer).then(function (res) {
                return res.data.data;
            });
        },

        addPlayerToLeague: function addPlayerToLeague(idLeague, idPlayer) {
            return axios.put(baseUrl + '/league/' + idLeague + '/add-player/' + idPlayer).then(function (res) {
                return res.data.data;
            });
        },

        generateTeams: function generateTeams(idLeague) {
            return axios.put(baseUrl + '/league/' + idLeague + '/generate-teams').then(function (res) {
                return res.data;
            });
        },

        removeTeams: function removeTeams(idLeague) {
            return axios.put(baseUrl + '/league/' + idLeague + '/remove-teams').then(function (res) {
                return res.data;
            });
        },

        editTeams: function editTeams(idLeague, teams) {
            return axios.put(baseUrl + '/league/' + idLeague + '/edit-teams', { teams: teams }).then(function (res) {
                console.log('res->', res);
                return res.data;
            });
        },

        generateMatches: function generateMatches(idLeague) {
            return axios.put(baseUrl + '/league/' + idLeague + '/generate-matches').then(function (res) {
                return res.data;
            });
        },

        removeMatches: function removeMatches(idLeague) {
            return axios.put(baseUrl + '/league/' + idLeague + '/remove-matches').then(function (res) {
                return res.data;
            });
        },

        getMatchResult: function getMatchResult(idLeague, idMatch, idTeamWinner, gamesTeamWinner, idTeamLoser, gamesTeamLoser) {
            return axios.put(baseUrl + '/league/' + idLeague + '/match-result/' + idMatch, { idTeamWinner: idTeamWinner, gamesTeamWinner: gamesTeamWinner, idTeamLoser: idTeamLoser, gamesTeamLoser: gamesTeamLoser }).then(function (res) {
                return res.data;
            });
        }

    };
})();

exports.default = api_client;
